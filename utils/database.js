const BASE_URL =
  "http://213.190.4.40/administrator/index.php/services/service/Chat";
const axios = require("axios");

const checkRoom = async function (idUser) {
  const result = await axios
    .get(`${BASE_URL}/checkRoomChat/${idUser}`)
    .then((response) => {
      return response.data.data;
    })
    .catch((error) => {
      console.log("check error: " + error);
    });
  return result;
};

const createRoom = async function (idUser) {
  const result = await axios
    .get(`${BASE_URL}/createRoomChat/${idUser}`)
    .then((response) => {
      return response.data.data;
    })
    .catch((error) => {
      console.log("create error: " + error);
    });
  return result;
};

module.exports = { checkRoom, createRoom };
