const chatForm = document.getElementById("chat-form");
const chatMessages = document.querySelector(".chat-messages");
const roomName = document.getElementById("room-name");
const userList = document.getElementById("users");
let token = null;

// Get username and room from URL
const { id } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

const socket = io();

// Get room and users
socket.on("notification/transaction", (transaction) => {
  console.log(transaction);
});

socket.on("notification/announcement", (announcement) => {
  console.log(announcement);
});

socket.on("notification", (notification) => {
  console.log(notification);
});

// Join chatroom
socket.emit("joinRoom", id);

// Get room and users
socket.on("infoRoom", (dataRoom) => {
  token = dataRoom.chat_token;
  console.log(dataRoom);
});

// Message from server
socket.on("message", ({ msg, isUser }) => {
  if (isUser) console.log("chat user : ", msg);
  else console.log("chat admin : ", msg);
  outputMessage(msg);

  // Scroll down
  chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Get Info Online
socket.on("userOnline", (status) => {
  console.log("status admin : " + status);
});

// Message submit
chatForm.addEventListener("submit", (e) => {
  e.preventDefault();

  // Get message
  const msg = e.target.elements.msg.value;

  // Emit message to server
  socket.emit("chatMessage", { token, msg, isUser: true });

  // Clear input
  e.target.elements.msg.value = "";
  e.target.elements.msg.focus();
});

// Output message to DOM
function outputMessage(message) {
  const div = document.createElement("div");
  div.classList.add("message");
  div.innerHTML = `<p class="meta">${message.username} <span>${message.time}</span></p>
    <p class="text">
        ${message.text}
    </p>`;
  document.querySelector(".chat-messages").appendChild(div);
}
