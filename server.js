const moment = require("moment");
const path = require("path");
const PORT = 2500 || process.env.PORT;

const express = require("express");
const app = express();

const http = require("http");
const server = http.createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

const { createRoom, checkRoom } = require("./utils/database.js");

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Run when clien connect
io.on("connection", (socket) => {
  socket.on("joinRoom", async (idUser) => {
    let dataRoom = await checkRoom(idUser);
    if (dataRoom === undefined) dataRoom = await createRoom(idUser);
    socket.join(dataRoom.chat_token);
    io.to(dataRoom.chat_token).emit("infoRoom", dataRoom);
  });

  // Select Room
  socket.on("selectRoom", async (token) => {
    socket.join(token);
    io.to(token).emit("userOnline", true);
  });

  // Listen for chatMessage
  socket.on("chatMessage", ({ token, msg, isUser }) => {
    io.to(token).emit("message", {
      msg,
      isUser,
      time: moment().format("YYYY-MM-DD HH:mm:ss"),
    });
  });

  // Listen for Notification
  socket.on("notification:global", (notification) => {
    io.emit("notification", notification);
  });

  // Listen for Notification Transaction
  socket.on("notification:transaction", (transaction) => {
    io.emit("notification/transaction", transaction);
  });

  // Listen for Notification Announcement
  socket.on("notification:announcement", (announcement) => {
    console.log(announcement)
    io.emit("notification/announcement", announcement);
  });

  // Run when client disconnect
  socket.on("leave", (token) => {
    io.to(token).emit("userOnline", false);
  });
});

server.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});
